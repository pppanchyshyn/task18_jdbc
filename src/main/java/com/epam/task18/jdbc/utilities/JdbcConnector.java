package com.epam.task18.jdbc.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcConnector {

  private static Properties properties = new Properties();
  private static Connection connection;

  private JdbcConnector() {
  }

  private static void loadProperties(){
    try {
      properties.load(new FileInputStream("src/main/resources/connection.properties"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static Connection getConnection() {
    loadProperties();
    if (connection == null) {
      try {
        connection = DriverManager
            .getConnection(properties.getProperty("url"), properties.getProperty("user"),
                properties.getProperty("password"));
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
    return connection;
  }

  public static Connection getTestConnection() {
    loadProperties();
      try {
        connection = DriverManager
            .getConnection(properties.getProperty("url"), properties.getProperty("user"),
                properties.getProperty("password"));
      } catch (SQLException e) {
        e.printStackTrace();
      }
    return connection;
  }
}

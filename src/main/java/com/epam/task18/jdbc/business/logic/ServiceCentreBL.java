package com.epam.task18.jdbc.business.logic;

import com.epam.task18.jdbc.dao.implementation.ServiceCentreDaoImp;
import com.epam.task18.jdbc.model.ServiceCentre;
import java.sql.SQLException;
import java.util.List;

public class ServiceCentreBL {

  public ServiceCentreBL() {
  }
  public List<ServiceCentre> getAll() {
    List<ServiceCentre> serviceCentres = null;
    try {
      serviceCentres = new ServiceCentreDaoImp().getAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return serviceCentres;
  }

  public ServiceCentre getByKey(Integer key) {
    ServiceCentre serviceCentre = null;
    try {
      serviceCentre = new ServiceCentreDaoImp().getByKey(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return serviceCentre;
  }

  public int create(ServiceCentre entity) {
    int result = 0;
    try {
      result =  new ServiceCentreDaoImp().create(entity);
    } catch (SQLException e) {
      e.getErrorCode();
    }
    return result;
  }

  public int update(ServiceCentre entity) {
    int result = 0;
    try {
      result =  new ServiceCentreDaoImp().update(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int delete(Integer key) {
    int result = 0;
    try {
      result = new ServiceCentreDaoImp().delete(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }
}

package com.epam.task18.jdbc.business.logic;

import com.epam.task18.jdbc.dao.implementation.ClientDaoImp;
import com.epam.task18.jdbc.model.Client;
import java.sql.SQLException;
import java.util.List;

public class ClientBL {

  public ClientBL() {
  }

  public List<Client> getAll() {
    List<Client> clients = null;
    try {
      clients = new ClientDaoImp().getAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return clients;
  }

  public Client getByKey(Integer key) {
    Client client = null;
    try {
      client = new ClientDaoImp().getByKey(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return client;
  }

  public int create(Client entity) {
    int result = 0;
    try {
     result = new ClientDaoImp().create(entity);
    } catch (SQLException e) {
      e.getErrorCode();
    }
    return result;
  }

  public int update(Client entity) {
    int result = 0;
    try {
      result = new ClientDaoImp().update(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int delete(Integer key) {
    int result = 0;
    try {
      result = new ClientDaoImp().delete(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public List<Client> getByMobileOperator(Integer mobileOperatorId) {
    List<Client> clients = null;
    try {
      clients =  new ClientDaoImp().getByMobileOperator(mobileOperatorId);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return clients;
  }
}

package com.epam.task18.jdbc.business.logic;

import com.epam.task18.jdbc.dao.implementation.TariffTermsDaoImp;
import com.epam.task18.jdbc.model.TariffTerms;
import java.sql.SQLException;
import java.util.List;

public class TariffTermsBL {

  public TariffTermsBL() {
  }
  public List<TariffTerms> getAll() {
    List<TariffTerms> tariffTermsList = null;
    try {
      tariffTermsList =  new TariffTermsDaoImp().getAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return tariffTermsList;
  }

  public TariffTerms getByKey(Integer key) {
    TariffTerms tariffTerms = null;
    try {
      tariffTerms = new TariffTermsDaoImp().getByKey(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return tariffTerms;
  }

  public int create(TariffTerms entity) {
    int result = 0;
    try {
      result = new TariffTermsDaoImp().create(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int update(TariffTerms entity) {
    int result = 0;
    try {
      result =  new TariffTermsDaoImp().update(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int delete(Integer key) {
    int result = 0;
    try {
      result = new TariffTermsDaoImp().delete(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }
}

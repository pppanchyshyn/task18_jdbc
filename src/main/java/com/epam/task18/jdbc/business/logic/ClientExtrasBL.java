package com.epam.task18.jdbc.business.logic;

import com.epam.task18.jdbc.dao.implementation.ClientExtrasDaoImp;
import com.epam.task18.jdbc.model.ClientExtras;
import com.epam.task18.jdbc.model.ClientExtrasPrimaryKey;
import java.sql.SQLException;
import java.util.List;

public class ClientExtrasBL {

  public ClientExtrasBL() {
  }

  public List<ClientExtras> getAll() {
    List<ClientExtras> clientExtrasList = null;
    try {
      clientExtrasList = new ClientExtrasDaoImp().getAll();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return clientExtrasList;
  }

  public int create(ClientExtras entity) {
    int result = 0;
    try {
      result =  new ClientExtrasDaoImp().create(entity);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public int delete(ClientExtrasPrimaryKey key) {
    int result = 0;
    try {
      result =  new ClientExtrasDaoImp().delete(key);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return result;
  }

  public List<ClientExtras> getByClientId(Integer clientId) {
    List<ClientExtras> clientExtrasList = null;
    try {
      clientExtrasList = new ClientExtrasDaoImp().getByClientId(clientId);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return clientExtrasList;
  }

  public List<ClientExtras> getByExtrasId(Integer extrasId) {
    List<ClientExtras> clientExtrasList = null;
    try {
      clientExtrasList =  new ClientExtrasDaoImp().getByExtrasId(extrasId);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return clientExtrasList;
  }
}

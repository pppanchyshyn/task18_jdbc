package com.epam.task18.jdbc.dao.implementation;

import com.epam.task18.jdbc.dao.interfaces.ServiceCentreDao;
import com.epam.task18.jdbc.model.ServiceCentre;
import com.epam.task18.jdbc.utilities.JdbcConnector;
import com.epam.task18.jdbc.utilities.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ServiceCentreDaoImp implements ServiceCentreDao {

  private static final String GET_ALL = "SELECT * FROM service_centre";
  private static final String GET_BY_KEY = "SELECT * FROM service_centre WHERE id=?";
  private static final String CREATE = "INSERT service_centre (id, city, address, operator_id) "
      + "VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE service_centre SET city=?, address=?, operator_id=? "
      + "WHERE id=?";
  private static final String DELETE = "DELETE FROM service_centre WHERE id=?";

  @Override
  public List<ServiceCentre> getAll() throws SQLException {
    List<ServiceCentre> centres = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_ALL)) {
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          centres.add((ServiceCentre) new Transformer(ServiceCentre.class)
              .transform(resultSet));
        }
      }
    }
    return centres;
  }

  @Override
  public ServiceCentre getByKey(Integer key) throws SQLException {
    ServiceCentre entity = null;
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_KEY)) {
      ps.setInt(1, key);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity = (ServiceCentre) new Transformer(ServiceCentre.class)
              .transform(resultSet);
          break;
        }
      }
    }
    return entity;
  }

  @Override
  public int create(ServiceCentre entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
      ps.setInt(1, entity.getId());
      ps.setString(2, entity.getCity());
      ps.setString(3, entity.getAddress());
      ps.setInt(4, entity.getOperatorId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(ServiceCentre entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
      ps.setString(1, entity.getCity());
      ps.setString(2, entity.getAddress());
      ps.setInt(3, entity.getOperatorId());
      ps.setInt(4, entity.getId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(Integer key) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
      ps.setInt(1, key);
      return ps.executeUpdate();
    }
  }
}
package com.epam.task18.jdbc.dao.implementation;

import com.epam.task18.jdbc.dao.interfaces.TariffTermsDao;
import com.epam.task18.jdbc.model.TariffTerms;
import com.epam.task18.jdbc.utilities.JdbcConnector;
import com.epam.task18.jdbc.utilities.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TariffTermsDaoImp implements TariffTermsDao {

  private static final String GET_ALL = "SELECT * FROM tariff_terms";
  private static final String GET_BY_KEY = "SELECT * FROM tariff_terms WHERE tariff_id=?";
  private static final String CREATE = "INSERT tariff_terms (id, tariff_id, minutes, sms, internet) VALUES (?, ?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE tariff_terms SET id=?, minutes=?, sms=?, internet=? WHERE tariff_id=?";
  private static final String DELETE = "DELETE FROM tariff_terms WHERE tariff_id=?";

  @Override
  public List<TariffTerms> getAll() throws SQLException {
    List<TariffTerms> terms = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_ALL)) {
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          terms.add((TariffTerms) new Transformer(TariffTerms.class)
              .transform(resultSet));
        }
      }
    }
    return terms;
  }

  @Override
  public TariffTerms getByKey(Integer key) throws SQLException {
    TariffTerms entity = null;
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_KEY)) {
      ps.setInt(1, key);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity = (TariffTerms) new Transformer(TariffTerms.class)
              .transform(resultSet);
          break;
        }
      }
    }
    return entity;
  }

  @Override
  public int create(TariffTerms entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
      ps.setInt(1, entity.getId());
      ps.setInt(2, entity.getTariffId());
      ps.setInt(3, entity.getMinutes());
      ps.setInt(4, entity.getSms());
      ps.setInt(5, entity.getInternet());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(TariffTerms entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
      ps.setInt(1, entity.getId());
      ps.setInt(2, entity.getMinutes());
      ps.setInt(3, entity.getSms());
      ps.setInt(4, entity.getInternet());
      ps.setInt(5, entity.getTariffId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(Integer key) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
      ps.setInt(1, key);
      return ps.executeUpdate();
    }
  }
}

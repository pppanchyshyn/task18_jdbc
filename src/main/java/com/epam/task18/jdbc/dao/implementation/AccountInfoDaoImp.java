package com.epam.task18.jdbc.dao.implementation;

import com.epam.task18.jdbc.dao.interfaces.AccountInfoDao;
import com.epam.task18.jdbc.model.AccountInfo;
import com.epam.task18.jdbc.utilities.JdbcConnector;
import com.epam.task18.jdbc.utilities.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountInfoDaoImp implements AccountInfoDao {

  private static final String GET_ALL = "SELECT * FROM account_info";
  private static final String GET_BY_KEY = "SELECT * FROM account_info WHERE client_id=?";
  private static final String CREATE = "INSERT account_info (id, client_id, balance, number_validity) VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE account_info SET id=?, balance=?, number_validity=? WHERE client_id=?";
  private static final String DELETE = "DELETE FROM account_info WHERE client_id=?";

  @Override
  public List<AccountInfo> getAll() throws SQLException {
    List<AccountInfo> info = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_ALL)) {
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          info.add((AccountInfo) new Transformer(AccountInfo.class)
              .transform(resultSet));
        }
      }
    }
    return info;
  }

  @Override
  public AccountInfo getByKey(Integer key) throws SQLException {
    AccountInfo entity = null;
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_KEY)) {
      ps.setInt(1, key);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity = (AccountInfo) new Transformer(AccountInfo.class)
              .transform(resultSet);
          break;
        }
      }
    }
    return entity;
  }

  @Override
  public int create(AccountInfo entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
      ps.setInt(1, entity.getId());
      ps.setInt(2, entity.getClientId());
      ps.setDouble(3, entity.getBalance());
      ps.setDate(4, entity.getNumberValidity());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(AccountInfo entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
      ps.setInt(1, entity.getId());
      ps.setDouble(2, entity.getBalance());
      ps.setDate(3, entity.getNumberValidity());
      ps.setInt(4, entity.getClientId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int delete(Integer key) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
      ps.setInt(1, key);
      return ps.executeUpdate();
    }
  }
}

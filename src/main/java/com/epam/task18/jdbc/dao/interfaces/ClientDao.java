package com.epam.task18.jdbc.dao.interfaces;

import com.epam.task18.jdbc.model.Client;
import java.sql.SQLException;
import java.util.List;

public interface ClientDao extends Dao<Client, Integer> {

  List<Client> getByMobileOperator(Integer mobileOperatorId) throws SQLException;
}

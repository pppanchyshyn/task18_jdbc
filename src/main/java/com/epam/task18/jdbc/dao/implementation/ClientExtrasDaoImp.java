package com.epam.task18.jdbc.dao.implementation;

import com.epam.task18.jdbc.dao.interfaces.ClientExtrasDao;
import com.epam.task18.jdbc.model.ClientExtras;
import com.epam.task18.jdbc.model.ClientExtrasPrimaryKey;
import com.epam.task18.jdbc.utilities.JdbcConnector;
import com.epam.task18.jdbc.utilities.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientExtrasDaoImp implements ClientExtrasDao {

  private static final String GET_ALL = "SELECT * FROM client_extras";
  private static final String GET_BY_EXTRAS_ID = "SELECT * FROM client_extras WHERE extras_id=?";
  private static final String GET_BY_CLIENT_ID = "SELECT * FROM client_extras WHERE client_id=?";
  private static final String CREATE = "INSERT client_extras (client_id, extras_id) VALUES (?, ?)";
  private static final String DELETE = "DELETE FROM client_extras WHERE client_id=? AND extras_id=?";

  @Override
  public List<ClientExtras> getAll() throws SQLException {
    List<ClientExtras> clientExtrasList = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_ALL)) {
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          clientExtrasList.add((ClientExtras) new Transformer(ClientExtras.class)
              .transform(resultSet));
        }
      }
    }
    return clientExtrasList;
  }

  @Override
  public ClientExtras getByKey(ClientExtrasPrimaryKey key) throws SQLException {
    return null;
  }

  public List<ClientExtras> getByExtrasId(Integer extrasId) throws SQLException {
    List<ClientExtras> clientExtrasList = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_EXTRAS_ID)) {
      ps.setInt(1, extrasId);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          clientExtrasList.add((ClientExtras) new Transformer(ClientExtras.class)
              .transform(resultSet));
        }
      }
    }
    return clientExtrasList;
  }

  public List<ClientExtras> getByClientId(Integer clientId) throws SQLException {
    List<ClientExtras> clientExtrasList = new ArrayList<>();
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(GET_BY_CLIENT_ID)) {
      ps.setInt(1, clientId);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          clientExtrasList.add((ClientExtras) new Transformer(ClientExtras.class)
              .transform(resultSet));
        }
      }
    }
    return clientExtrasList;
  }

  @Override
  public int create(ClientExtras entity) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
      ps.setInt(1, entity.getClientExtrasPrimaryKey().getClientId());
      ps.setInt(2, entity.getClientExtrasPrimaryKey().getExtrasId());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(ClientExtras entity) throws SQLException {
    return 0;
  }

  @Override
  public int delete(ClientExtrasPrimaryKey key) throws SQLException {
    Connection connection = JdbcConnector.getConnection();
    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
      ps.setInt(1, key.getClientId());
      ps.setInt(2, key.getExtrasId());
      return ps.executeUpdate();
    }
  }
}

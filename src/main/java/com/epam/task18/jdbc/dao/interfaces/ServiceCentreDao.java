package com.epam.task18.jdbc.dao.interfaces;

import com.epam.task18.jdbc.model.ServiceCentre;

public interface ServiceCentreDao extends Dao<ServiceCentre, Integer> {

}

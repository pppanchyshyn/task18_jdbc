package com.epam.task18.jdbc.controller;

import com.epam.task18.jdbc.business.logic.MobileOperatorBL;
import com.epam.task18.jdbc.model.MobileOperator;
import java.util.List;

public class MobileOperatorController extends AbstractController{

  void getOperatorTable() {
    logger.info("\nMobile Operator:");
    MobileOperatorBL mobileOperatorBL = new MobileOperatorBL();
    List<MobileOperator> operators = mobileOperatorBL.getAll();
    for (MobileOperator operator : operators) {
      logger.info(operator);
    }
  }

  void getOperatorById() {
    logger.info("Enter id of Mobile Operator: ");
    Integer id = enterInteger();
    MobileOperatorBL mobileOperatorBL = new MobileOperatorBL();
    MobileOperator operator = mobileOperatorBL.getByKey(id);
    logger.info(operator);
  }

  void createNewOperator() {
    logger.info("Input Mobile Operator id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Mobile Operator name: ");
    String name = input.nextLine();
    MobileOperator operator = new MobileOperator(id, name);
    MobileOperatorBL bl = new MobileOperatorBL();
    int count = bl.create(operator);
    logger.info(String.format("%d row created", count));
  }

 void updateOperator() {
    logger.info("Input Mobile Operator id: ");
    Integer id = enterInteger();
    input.nextLine();
    logger.info("Input Mobile Operator name: ");
    String name = input.nextLine();
    MobileOperator operator = new MobileOperator(id, name);
    MobileOperatorBL bl = new MobileOperatorBL();
    int count = bl.create(operator);
    logger.info(String.format("%d row was updated", count));
  }

  void deleteOperatorByID() {
    logger.info("Input Mobile Operator id: ");
    Integer id = enterInteger();
    input.nextLine();
    MobileOperatorBL bl = new MobileOperatorBL();
    int count = bl.delete(id);
    logger.info(String.format("%d row was deleted", count));
  }
}

package com.epam.task18.jdbc.controller;

import com.epam.task18.jdbc.business.logic.ClientExtrasBL;
import com.epam.task18.jdbc.model.ClientExtras;
import com.epam.task18.jdbc.model.ClientExtrasPrimaryKey;
import java.util.List;

public class ClientExtrasController extends AbstractController {

  void getClientExtrasTable() {
    logger.info("\nClient Extras:");
    ClientExtrasBL bl = new ClientExtrasBL();
    List<ClientExtras> clientExtrasList = bl.getAll();
    for (ClientExtras clientExtras : clientExtrasList) {
      logger.info(clientExtras);
    }
  }

  void getClientExtrasByClient() {
    logger.info("Enter client_id for Client Extras: ");
    Integer id = enterInteger();
    input.nextLine();
    ClientExtrasBL bl = new ClientExtrasBL();
    List<ClientExtras> clientExtrasList = bl.getByClientId(id);
    for (ClientExtras clientExtras : clientExtrasList) {
      logger.info(clientExtras);
    }
  }

  void getClientExtrasByExtas() {
    logger.info("Enter extras_id for Client Extras: ");
    Integer id = enterInteger();
    input.nextLine();
    ClientExtrasBL bl = new ClientExtrasBL();
    List<ClientExtras> clientExtrasList = bl.getByExtrasId(id);
    for (ClientExtras clientExtras : clientExtrasList) {
      logger.info(clientExtras);
    }
  }

  void createClientExtras() {
    logger.info("Input client_id for ClientExtras: ");
    Integer clientId = enterInteger();
    input.nextLine();
    logger.info("Input extras_id for ClientExtras: ");
    Integer extrasId = enterInteger();
    input.nextLine();
    ClientExtrasPrimaryKey pk = new ClientExtrasPrimaryKey(clientId, extrasId);
    ClientExtras clientExtras = new ClientExtras(pk);
    ClientExtrasBL bl = new ClientExtrasBL();
    int count = bl.create(clientExtras);
    logger.info(String.format("%d row created", count));
  }

  void deleteClientExtras() {
    logger.info("Input client_id for ClientExtras: ");
    Integer clientId = enterInteger();
    input.nextLine();
    logger.info("Input extras_id for ClientExtras: ");
    Integer extrasId = enterInteger();
    input.nextLine();
    ClientExtrasPrimaryKey pk = new ClientExtrasPrimaryKey(clientId, extrasId);
    ClientExtrasBL bl = new ClientExtrasBL();
    int count = bl.delete(pk);
    logger.info(String.format("%d row was deleted", count));
  }
}

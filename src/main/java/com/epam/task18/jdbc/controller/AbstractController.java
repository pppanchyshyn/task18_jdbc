package com.epam.task18.jdbc.controller;

import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class AbstractController {

  Logger logger = LogManager.getLogger(AbstractController.class);
  Scanner input = new Scanner(System.in);

  int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        logger.info("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  double enterDouble() {
    double value;
    while (true) {
      if (input.hasNextDouble()) {
        value = input.nextDouble();
        break;
      } else {
        logger.info("Not Double! Try again: ");
        input.nextLine();
      }
    }
    return value;
  }
}

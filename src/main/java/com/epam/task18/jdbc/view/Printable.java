package com.epam.task18.jdbc.view;

@FunctionalInterface
public interface Printable {

  void print();
}

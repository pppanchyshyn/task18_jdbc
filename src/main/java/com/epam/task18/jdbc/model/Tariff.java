package com.epam.task18.jdbc.model;

import com.epam.task18.jdbc.utilities.annotations.Column;
import com.epam.task18.jdbc.utilities.annotations.PrimaryKey;
import com.epam.task18.jdbc.utilities.annotations.Table;

@Table(name = "tariff")
public class Tariff {

  @PrimaryKey
  @Column(name = "id")
  private Integer id;
  @Column(name = "name")
  private String name;
  @Column(name = "fee")
  private Integer fee;
  @Column(name = "operator_id")
  private Integer operatorId;

  public Tariff() {
  }

  public Tariff(Integer id, String name, Integer fee, Integer operatorId) {
    this.id = id;
    this.name = name;
    this.fee = fee;
    this.operatorId = operatorId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getFee() {
    return fee;
  }

  public void setFee(Integer fee) {
    this.fee = fee;
  }

  public Integer getOperatorId() {
    return operatorId;
  }

  public void setOperatorId(Integer operatorId) {
    this.operatorId = operatorId;
  }

  @Override
  public String toString() {
    return String.format("%-11d%-20s%-11d%-11d", id, name, fee, operatorId);
  }
}

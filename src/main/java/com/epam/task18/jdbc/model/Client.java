package com.epam.task18.jdbc.model;

import com.epam.task18.jdbc.utilities.annotations.Column;
import com.epam.task18.jdbc.utilities.annotations.PrimaryKey;
import com.epam.task18.jdbc.utilities.annotations.Table;

@Table(name = "client")
public class Client {
  @PrimaryKey()
  @Column(name = "id")
  private Integer id;
  @Column(name = "number")
  private String number;
  @Column(name = "name")
  private String name;
  @Column(name = "tariff_id")
  private Integer tariffId;

  public Client() {
  }

  public Client(Integer id, String number, String name, Integer tariffId) {
    this.id = id;
    this.number = number;
    this.name = name;
    this.tariffId = tariffId;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getTariffId() {
    return tariffId;
  }

  public void setTariffId(Integer tariffId) {
    this.tariffId = tariffId;
  }

  @Override
  public String toString() {
    return String.format("%-11d%-20s%-20s%-11d", id, number, name, tariffId);
  }
}

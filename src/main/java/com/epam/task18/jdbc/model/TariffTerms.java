package com.epam.task18.jdbc.model;

import com.epam.task18.jdbc.utilities.annotations.Column;
import com.epam.task18.jdbc.utilities.annotations.PrimaryKey;
import com.epam.task18.jdbc.utilities.annotations.Table;

@Table(name = "tariff_terms")
public class TariffTerms {

  @PrimaryKey()
  @Column(name = "id")
  private Integer id;
  @Column(name = "tariff_id")
  private Integer tariffId;
  @Column(name = "minutes")
  private Integer minutes;
  @Column(name = "sms")
  private Integer sms;
  @Column(name = "internet")
  private Integer internet;

  public TariffTerms() {
  }

  public TariffTerms(Integer id, Integer tariffId, Integer minutes, Integer sms,
      Integer internet) {
    this.id = id;
    this.tariffId = tariffId;
    this.minutes = minutes;
    this.sms = sms;
    this.internet = internet;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getTariffId() {
    return tariffId;
  }

  public void setTariffId(Integer tariffId) {
    this.tariffId = tariffId;
  }

  public Integer getMinutes() {
    return minutes;
  }

  public void setMinutes(Integer minutes) {
    this.minutes = minutes;
  }

  public Integer getSms() {
    return sms;
  }

  public void setSms(Integer sms) {
    this.sms = sms;
  }

  public Integer getInternet() {
    return internet;
  }

  public void setInternet(Integer internet) {
    this.internet = internet;
  }

  @Override
  public String toString() {
    return String
        .format("%-11d%-11d%-11d%-11d%-11d", id, tariffId, minutes, sms, internet);
  }
}

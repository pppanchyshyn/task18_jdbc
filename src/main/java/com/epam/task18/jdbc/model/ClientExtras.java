package com.epam.task18.jdbc.model;

import com.epam.task18.jdbc.utilities.annotations.CompositePrimaryKey;
import com.epam.task18.jdbc.utilities.annotations.Table;

@Table(name = "client_extras")
public class ClientExtras {

  @CompositePrimaryKey
  private ClientExtrasPrimaryKey clientExtrasPrimaryKey;

  public ClientExtras() {
  }

  public ClientExtras(ClientExtrasPrimaryKey clientExtrasPrimaryKey) {
    this.clientExtrasPrimaryKey = clientExtrasPrimaryKey;
  }

  public ClientExtrasPrimaryKey getClientExtrasPrimaryKey() {
    return clientExtrasPrimaryKey;
  }

  public void setClientExtrasPrimaryKey(
      ClientExtrasPrimaryKey clientExtrasPrimaryKey) {
    this.clientExtrasPrimaryKey = clientExtrasPrimaryKey;
  }

  @Override
  public String toString() {
    return clientExtrasPrimaryKey.toString();
  }
}

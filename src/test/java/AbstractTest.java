import com.epam.task18.jdbc.utilities.JdbcConnector;
import java.sql.Connection;
import java.sql.SQLException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

abstract class AbstractTest {

  private static Connection connection;
  final int successfulOperationValue = 1;

  @BeforeMethod
  void setUp() {
    connection = JdbcConnector.getTestConnection();
    try {
      connection.setAutoCommit(false);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @AfterMethod
  void tirDown() {
    try {
      connection.rollback();
      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}

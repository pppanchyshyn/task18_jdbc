import com.epam.task18.jdbc.business.logic.ExtrasBL;
import com.epam.task18.jdbc.model.Extras;
import com.epam.task18.jdbc.view.ConsoleView;
import org.testng.Assert;
import org.testng.annotations.Test;

class ExtrasBLTest extends AbstractTest {

  private ExtrasBL extrasBL = new ExtrasBL();

  @Test
  void testGetByKey() {
    ConsoleView.logger.info("Getting extra by id is testing...");
    int id = 2;
    String extrasName = "Djingle";
    Assert.assertEquals(extrasName, extrasBL.getByKey(id).getName(), "Can't find extra by id");
  }

  @Test
  void testCreatePositive() {
    ConsoleView.logger.info("Creating new extra is testing...");
    Extras testedExtra = new Extras(20, "New Extra", "Some description", 1, 3);
    extrasBL.create(testedExtra);
    Assert.assertNotNull(extrasBL.getByKey(testedExtra.getId()), "extra isn't created");
    Assert.assertEquals(testedExtra.getDescription(),
        extrasBL.getByKey(testedExtra.getId()).getDescription(), "extra was created wrong");
  }

  @Test
  void testCreateNegative() {
    ConsoleView.logger.info("Fail of creating extra with invalid data is testing...");
    Extras testedExtra = new Extras(20, "Djingle", "Some description", 1, 3);
    Assert.assertNotEquals(successfulOperationValue, extrasBL.create(testedExtra),
        "extra with invalid data was created");
  }

  @Test
  void testDelete() {
    ConsoleView.logger.info("Extra deleting is testing...");
    int deletedId = 1;
    extrasBL.delete(deletedId);
    Assert.assertNull(extrasBL.getByKey(deletedId), "Extra wasn't deleted");
  }
}

